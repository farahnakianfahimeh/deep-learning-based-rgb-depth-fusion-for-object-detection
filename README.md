# RGB-depth Fusion Framework for Object Detection in Autonomous Vehicles


Reliable object detection is a key research goal in robotics and computer vision as it would greatly facilitate the practi- cal deployment of autonomous driving. In fact, autonomous vehicles provide the sufficient autonomy and safety level if they able to robustly detect the interest objects in their perception system. Object detection and classification have been extensively studied for RGB images, and there are open- source large datasets available. The main reason is that RGB cameras are the cheapest and most commonly used type of sensors for the detection of objects. Cameras provide high resolution images about the scene with dense texture and semantic information.


Because of lack of depth information in RGB images, more recent papers exploring the addition of depth data for improving object detection. Depth images are invariant to lighting and allow better background separation. Moreover, they contain additional information about the shape of objects. Therefore, combining the information from these two sensors via sensor fusion allows for the robust integration of this complementary information. On the other hand, generating high quality depth map from 2D images is attractive because it could inexpensively complement LiDAR (Light Detection And Ranging) sensors used in autonomous vehicles. In addition, depth estimation is critical step in scene reconstruction, object detection and segmentation for autonomous driving. In this application, recovery of depth information is more important when there is no information such as stereo images or point clouds are unavailable.

We  aim to answer the question of how much would a successful solution to monocular depth inference aid to object detection? In order to address this question, we present an early fusion framework based on CNN for object detection from RGB-D data. The fusion framework leverages the depth information to produce an image from RGB images using Monodepth [1] as a self-supervised depth estimation method. Then it uses RGB-D as inputs of a CNN- based detector to improve the accuracy of the object detection task. To evaluate the proposed framework, we use KITTI
benchmark dataset [2] which consists of around 7,500 stereo RGB images, captured from a car driving around a city and has annotations for cars, cyclists and pedestrians. We trained state-of-the art detectors using different configurations of RGB images and their depth image, in conjunction and 
independently.

# RGB-D based fusion framework

Fig.1 shows an illustration of the framework used for object detection. The framework first employs a depth estimation module to generate a depth image. Then it uses the detection module for performing detection on RGB-D images. The depth estimation and detection modules are described as follows:

- Depth Estimation: 
In order to depth estimation, we first experimented with a simple method from the literature: the algorithm, MonoDepth, proposed by Godard et al. [1] recently. This method attempts to perform depth estimation using a self-supervised learning with no manual labeling or even camera motion information.

- Detection: 
The framework has been structured to find complementary information from both RGB and depth images. For this purpose, the depth image from the previous depth estimation module is concatenated to RGB image at a very low abstraction level. The goal is to use information from intensity and depth images and make a joint decision regarding how likely are objects to be visible in each location of the image. Data can be fused early, at the feature level or late, at the decision level. we explored the early fusion that feature vectors extracted from both color and depth images are combined into one common feature vector, which is the input of a detector. The intuition behind this is simple, since the features of the concatenated images should contain both information from RGB and depth.
The Convolutional neural networks-based detector processes the concatenated input image with set of filters and produces the bounding boxes from the feature maps to localize the objects.

![Image description](framework.png)




[1] Cle ́ment Godard, Oisin Mac Aodha, and Gabriel J. Brostow. Digging into self-supervised monocular depth estimation. CoRR, abs/1806.01260, 2018.

[2] Andreas Geiger. Are we ready for autonomous driving? the kitti vision benchmark suite. In Proceedings of the 2012 IEEE Conference on Computer Vision and Pattern Recognition (CVPR), CVPR ’12, page 3354–3361, USA, 2012. IEEE Computer Society.
